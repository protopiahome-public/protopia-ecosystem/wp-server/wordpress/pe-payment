<?php

require_once(__DIR__ . "/../../pe-graphql/vendor/autoload.php");

use GraphQL\Error\ClientAware;
use GraphQL\Utils\BuildSchema;
use GraphQL\Utils\SchemaPrinter;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class pe_payment
{
	static function activate()
	{		
		$options = get_option(PESHOP);
		global $wpdb;			
		$query = "CREATE TABLE `".$wpdb->prefix."payment` (
  `ID` int(20) UNSIGNED NOT NULL,
  `user_id` int(20) UNSIGNED NOT NULL DEFAULT '0',
  `elemet_id` int(20) UNSIGNED NOT NULL DEFAULT '0',
  `summ` int(20) UNSIGNED NOT NULL DEFAULT '0',
  `date` datetime NOT NULL,
  `element_type` enum('post','taxonomy','user','option') NOT NULL DEFAULT 'post'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
";
		$wpdb->query($query);	
	}
	
	static function deactivate()
	{
		
	}
	
	static $options;	
	static $instance;
	
	static function get_instance()
	{
		if(!static::$instance)
			static::$instance = new static;
		return static::$instance;
	}
	
	function __construct()
	{		
		static::$options = get_option(PESHOP);
		add_action( 'admin_menu',					[ __CLASS__, 'admin_page_handler'], 9);
		add_filter( 'smc_add_post_types', 			[ __CLASS__, 'smc_add_post_types'], 9, 1);
		add_filter( 'template_include', 			[ __CLASS__, 'my_template'], 9, 1);
		add_filter( "ermak_body_script", 			[ __CLASS__, "ermak_body_script"], 9);
		add_filter( "ermak_body_before", 			[ __CLASS__, "ermak_body_header"], 9);
		add_action( 'wp_enqueue_scripts', 			[ __CLASS__, 'add_frons_js_script'],12 );
		add_action( 'admin_enqueue_scripts', 		[ __CLASS__, 'add_admin_js_script'] );
		add_filter( "get_all_matrixes", 			[ __CLASS__, 'get_all_matrixes_handler'], 10, 3); 
		add_filter( "get_single_matrix", 			[ __CLASS__, 'get_single_matrix_handler'], 10, 3); 
		add_filter( "pe_graphql_user_fields",		[ __CLASS__, "pe_graphql_user_fields"], 10, 2 );
		add_filter( "bio_get_user",					[ __CLASS__, "bio_get_user"], 10, 2 );
		add_filter( "pe_graphql_get_users",			[ __CLASS__, "pe_graphql_get_users"], 10, 2 );
		add_filter( "pe_graphql_get_user",			[ __CLASS__, "pe_graphql_get_user"]);
		add_filter( "pe_graphql_change_meta_user",	[ __CLASS__, "pe_graphql_change_meta_user"], 10, 2);
		add_action( "pe_graphql_change_current_user", [ __CLASS__, "pe_graphql_change_current_user"]);
		add_filter( "smc_add_post_types",	 		[ __CLASS__, "init_obj"], 20);
		add_action( "pe_graphql_make_schema", 		[ __CLASS__, "exec_graphql"], 50);
		add_action( 'user_register', 				[ __CLASS__, 'user_current_course'] );
		add_filter( 'shm_ajax_submit', 				[ __CLASS__, 'shm_ajax_submit'] );
		add_filter( 'bio_gq_change_bio_review', 	[ __CLASS__, 'bio_gq_change_bio_review'], 10, 2 );
		//
		add_filter( 'smc_edited_term',  			[__CLASS__, 'smc_edited_term'], 90, 3 );
		
		/*
		add_filter( "bio_gq_options", 					[ __CLASS__, "bio_gq_options"], 50);
		add_filter( "bio_gq_options_input", 			[ __CLASS__, "bio_gq_options_input"], 50);
		add_filter( "bio_get_gq_options", 				[ __CLASS__, "bio_get_gq_options"], 50);
		add_action( "bio_change_gq_options", 			[ __CLASS__, "bio_change_gq_options"], 50);
		*/
		
		add_action( BIO_COURSE_TYPE.'_edit_form_fields',[ __CLASS__, 'add_ctg'], 20, 2 );
		add_action( 'edit_'.BIO_COURSE_TYPE, 			[ __CLASS__, 'save_ctg'], 12);  
		add_action( 'create_'.BIO_COURSE_TYPE, 			[ __CLASS__, 'save_ctg'], 12);
		add_filter( "manage_edit-".BIO_COURSE_TYPE."_columns", 	array( __CLASS__,'ctg_columns')); 
		add_filter( "manage_".BIO_COURSE_TYPE."_custom_column",	array( __CLASS__,'manage_ctg_columns'), 12, 3);	
		
		add_action( 'create_'.BIO_FACULTET_TYPE, 		[ __CLASS__, 'create_facultet'], 12);
		add_filter( 'before_update_smc_taxonomy', 		[ __CLASS__, 'before_update_smc_taxonomy'], 12, 3);
		
		add_filter( "bio_admin",	array( __CLASS__,'bio_admin') );	
	}
	// добавить владельца Факультета в список доступа к Факультету
	static function create_facultet( $term_id  )
	{
		$facultet = Bio_Facultet::get_instance( $term_id );
		$post_author = $facultet->get_meta("post_author");
		if($post_author)
		{
			Bio_Facultet::add_user_facultet( $term_id, $post_author );
		}
	}
	static function before_update_smc_taxonomy( $data, $term_id, $type  )
	{
		if($type == Bio_Facultet::get_type())
		{
			if($data['post_author'])
			{
				$facultet = Bio_Facultet::get_instance( $term_id );
				Bio_Facultet::remove_user_facultet($term_id, $facultet->get_meta("post_author"));
				Bio_Facultet::add_user_facultet( $term_id, $data['post_author'] );
			}
		}
		return $data;
	}
	static function bio_admin($text)
	{
		require_once(_REAL_PATH."class/SMC_Object_type.php");
		$text .= "		
			<li class='list-group-item '>
				<div class='raw'>
					<div class='col-md-12 col-sm-12 mb-3 lead'>".
						__("Pay System", BIO).
					"</div>
					<div class='col-md-4 col-sm-12'>".
						
					"</div>
					<div class='col-md-8 col-sm-12 mb-2'>
						<input type='radio' class='radio bio_options' id='robocassa' name='pay_system' ".checked(1,Bio::$options["pay_system"],0) ." value='1'/>
						<label for='robocassa'>".__("Robocassa", BIO). "</label>
						<br/>
						<input type='radio' class='radio bio_options' id='yandex_money' name='pay_system' ".checked(2,Bio::$options["pay_system"],0) ." value='2'/>
						<label for='yandex_money'>".__("Yandex money", BIO). "</label>
					</div>					
				</div>
			</li>		
			<li class='list-group-item '>
				<div class='raw'>
					<div class='col-md-12 col-sm-12 mb-3 lead'>".
						__("Payments", BIO).
					"</div>
					<div class='col-md-4 col-sm-12'>".
						__("Payed SMC type", BIO).
					"</div>
					<div class='col-md-8 col-sm-12 mb-2'>".
						SMC_Object_Type::dropdown(
							[
								"class" 	=> "form-control bio_options",
								"selected"	=> Bio::$options["goods_type"],
								"name"		=> "goods_type"
							]
						).
					"</div>					
				</div>
			</li>
		";
		return $text;
	}
	static function exec_graphql()
	{
		
	}
	
	static function bio_gq_change_bio_review( $matrix, $post_object )
	{
		update_user_meta(get_current_user_id(), "is_waiting_review", false);
		return $matrix;
	}
	
	static function smc_edited_term( $post_id, $data, $taxonomy_name )
	{
		//wp_die($post_id);
		update_term_meta( $post_id, "price", $data['price'] );
		update_term_meta( $post_id, "raiting", $data['raiting'] );
		return $post_id;
	}
	static function bio_get_user($args, $user)
	{
		$args['start_course_date']		= get_user_meta($user_id, "start_course_date", true);
		$args['is_waiting_review']		= get_user_meta($user_id, "is_waiting_review", true);
		return $args;
	}
	static function user_current_course( $user_id)
	{
		$defultCourseID = Bio::$options['default_course'];
		$term = Bio_Course::get_instance($defultCourseID);
		MPayment::insert([
			"post_title"	=> $term->body->name,
			"post_content"	=> __("Успешный платёж", MCOURSES),
			"post_author"	=> $user_id,
			"summae"		=> 0,
			"element_id"	=> $defultCourseID,
			"element_type"	=> "Bio_Course",
			"is_success"	=> 1
		]);
		MPayment::db_insert($user_id, "Bio_Course", $defultCourseID, 0, $term->body->name, 1);
		static::set_user_current_course( $user_id, $defultCourseID);
		
	}
	static function set_user_current_course( $user_id, $courseID, $prevouseCourseID = -1)
	{
		update_user_meta( $user_id, "current_course", $courseID );
		update_user_meta( $user_id, "start_course_date", time() );
		if($prevouseCourseID > 0)
			update_user_meta( $user_id, "is_waiting_review", null );
	}
	
	static function init_obj($init_object)
	{
		$p					= [];
		$p['t']				= ['type' => 'post'];	
		$p['class']			= ['type' => 'MPayment'];		
		$p['element_id']	= ['type' => "number", "name" => __("Element ID", PESHOP)];
		$p['element_type']	= ['type' => "smc_object",  "name" => __("Element SMC Type", PESHOP)];
		$p['summae']		= ['type' => "number", "name" => __("Summae", PESHOP)];
		$p['is_success'] 	= ['type' => "boolean", "name" => __("Is success?", PESHOP)];
		$init_object[M_PAYMENT_TYPE]	= $p;
		
		$p					= [];
		$p['t']				= ['type' => 'post'];	
		$p['class']			= ['type' => 'Bio_Review'];
		$init_object[BIO_REVIEW_TYPE]	= $p;
		
		$init_object[BIO_COURSE_TYPE]['price'] = ['type' => "number", "name" => __("Price", PESHOP)];
		$init_object[BIO_COURSE_TYPE]['raiting'] = ['type' => "number", "name" => __("Raiting", PESHOP)];
		
		return $init_object;
	}
	
	
	static function admin_page_handler()
	{
		add_menu_page( 
			__('pe_payment', MCOURSES), 
			__('pe_payment', MCOURSES),
			'manage_options', 
			'pe_payment_page', 
			array(__CLASS__, 'get_admin'), 
			" ", // icon url  
			'2'
		);		
	}
	static function get_admin()
	{
		$html = "
		<style>
			section
			{
				margin:40px 0;
			}
		</style>
		
		<div class='section'>
			<div class='container'>
				<div class='row'>
					
					
				</div>
			</div>
		</div>";
		$html;
		require_once MCOURSES_REAL_PATH . "tpl/admin.php" ;
	}
	static function add_frons_js_script()
	{
		wp_register_style(MCOURSES, MCOURSES_URLPATH . 'assets/css/pe_payment.css', array());
		wp_enqueue_style( MCOURSES);
	}
	static function add_admin_js_script()
	{
		wp_register_style(MCOURSES, MCOURSES_URLPATH . 'assets/css/pe_payment.css', array());
		wp_enqueue_style( MCOURSES);
	}
	
	static function get_all_matrixes_handler( $matrixes, $type )
	{
		if($type == BIO_COURSE_TYPE  && is_user_logged_in() && !current_user_can("manage_options")) 
		{
			//карты, которые пользователь прошёл - больше ему не показываются
			$payments = MPayment::db_get_all( get_current_user_id() );
			$the_matrix = [];
			foreach($matrixes as $matrix)
			{
				$boo = true;
				foreach($payments as $payment)
				{
					if($payment['element_id'] == $matrix['id'])
					{
						$boo = false;
					}
				}
				if($boo) $the_matrix[] = $matrix;
			}
			return $the_matrix;
		}
		return $matrixes;
	}
	
	static function get_single_matrix_handler($matrix, $term, $type)
	{
		switch($type)
		{
			case Bio_Course::get_type():
				$ins						= Bio_Course::get_instance($term);	
				$matrix['raiting']			= (int)$ins->get_meta("raiting");
				$matrix['price']			= (int)$ins->get_meta("price");
				$facultet					= Bio_Facultet::get_instance($ins->get_meta("facultet"));
				$matrix['bio_facultet']		= $facultet->is_enabled() ? $facultet->get_single_matrix() : null;
				return $matrix;
		}
		
		return $matrix;
	}
	
	static function pe_graphql_user_fields( $fields, $isForInput )
	{
		$fields['current_course'] = $isForInput ? Type::int() : PEGraphql::object_type( "Bio_Course" );
		
		//payments
		$fields['payments'] 	= $isForInput ? Type::listOf(Type::int()) : Type::listOf(PEGraphql::object_type( "MPayment" ));
		$fields['payments_new'] = $isForInput ? Type::listOf(Type::int()) : Type::listOf(PEGraphql::object_type( "MPaymentNew" ));
		$fields['phone'] 		= Type::string() ;
		$fields['is_blocked'] 	= Type::boolean() ;
		$fields['is_waiting_review']	= Type::boolean() ;
		$fields['start_course_date']	= Type::int() ;
		
		return $fields;
	}
	static function pe_graphql_get_user($user)
	{
		/*
		$currentCourseID 		= get_user_meta($user->ID, "current_course", true);	
		$currentCourseID 		= $currentCourseID  ? $currentCourseID  : Bio::$options['default_course'];
		$curCourse				= Bio_Course::get_instance( $currentCourseID );
		$user->current_course	= $curCourse->get_single_matrix();
		*/
		$payments				= MPayment::get_all(null, null, null, null, null, null, null, null, $user->ID );
			$ps					= [];
			foreach($payments as $p)
			{
				$ps[]			= MPayment::get_single_matrix($p);
			}
			$user->payments		= $ps;
		$user->payments_new		= MPayment::db_get_all($user->ID);
		$user->phone			= get_user_meta($user->ID, "phone", true);	
		$user->is_blocked		= (bool)get_user_meta($user->ID, "is_blocked", true);		
		$user->start_course_date= (int)get_user_meta($user->ID, "start_course_date", true);	
		
		$is_waiting_review		= (bool)get_user_meta($user->ID, "is_waiting_review", true);	
		// if more than month
		if( time() - $user->start_course_date > WEEK_IN_SECONDS )
		{
			update_user_meta($user->ID, "is_waiting_review", true);
			$is_waiting_review	= true;
		}
		
		$user->is_waiting_review = $is_waiting_review;
		return $user;
	}
	static function pe_graphql_get_users($users)
	{
		foreach($users as $user)
		{
			$currentCourseID 		= get_user_meta($user->ID, "current_course", true);	
			$currentCourseID 		= $currentCourseID  ? $currentCourseID  : Bio::$options['default_course'];
			$curCourse				= Bio_Course::get_instance( $currentCourseID );
			$user->current_course	= $curCourse->get_single_matrix();
			
			//
			$payments			= MPayment::get_all( null, null, null, null, null, null, null, null, $user->ID );
			$ps					= [];
			foreach($payments as $p)
			{
				$ps[]			= MPayment::get_single_matrix($p);
			}
			$user->payments		= $ps;
			$user->payments_new		= MPayment::db_get_all($user->ID);
		}
		return $users;
	}
	static function pe_graphql_change_meta_user($user_id, $fields)
	{
		if( isset($fields['input']['phone']) )
		{
			update_user_meta($user_id, "phone", $fields['input']['phone'] );
		}
		if( isset($fields['input']['is_blocked']) )
		{
			update_user_meta($user_id, "is_blocked", $fields['input']['is_blocked'] );
		}
		if( isset($fields['input']['start_course_date']) )
		{
			update_user_meta($user_id, "start_course_date", $fields['input']['start_course_date'] );
		}
		if( isset($fields['input']['is_waiting_review']) )
		{
			update_user_meta($user_id, "is_waiting_review", $fields['input']['is_waiting_review'] );
		}
		if( isset($fields['input']['current_course']) )
		{
			$prev = get_user_meta($user_id, "current_course", true );
			static::set_user_current_course( $user_id, $fields['input']['current_course'], $prev);
		}
		return $user_id;
	}
	static function pe_graphql_change_current_user( $args)
	{
		update_user_meta( get_current_user_id(), "phone", $args['input']['phone'] );
	}
	
	
	
	static function bio_gq_options( $matrix )
	{	
		$matrix["default_course"]		= [
			'type' => PEGraphql::object_type( "Bio_Course" ), 	
			'description' 	=> __( 'default course', BIO ) 
		];
		$matrix["robocassa_key"]		= [
			'type' => Type::string(), 	
			'description' 	=> __( 'robocassa_key', BIO ) 
		];
		$matrix["robocassa_password_1"]	= [
			'type' => Type::string(), 	
			'description' 	=> __( 'robocassa_password_1', BIO ) 
		];
		$matrix["robocassa_password_2"]	= [
			'type' => Type::string(), 	
			'description' 	=> __( 'robocassa_password_2', BIO ) 
		];
		return $matrix;
	}
	static function bio_gq_options_input( $matrix )
	{		
		$matrix["default_course"]		= [
			'type' => Type::int(), 	
			'description' 	=> __( 'default course id', BIO ) 
		];
		$matrix["robocassa_key"]		= [
			'type' => Type::string(), 	
			'description' 	=> __( 'robocassa_key', BIO ) 
		];
		$matrix["robocassa_password_1"]	= [
			'type' => Type::string(), 	
			'description' 	=> __( 'robocassa_password_1', BIO ) 
		];
		$matrix["robocassa_password_2"]	= [
			'type' => Type::string(), 	
			'description' 	=> __( 'robocassa_password_2', BIO ) 
		];
		return $matrix;
	}
	static function bio_get_gq_options( $matrix )
	{
		$course 						= Bio_Course::get_instance( Bio::$options['default_course'] );
		$matrix["default_course"] 		= $course->get_single_matrix( );
		$matrix["robocassa_key"] 		= Bio::$options['robocassa_key'];
		$matrix["robocassa_password_1"]	= Bio::$options['robocassa_password_1'];
		$matrix["robocassa_password_2"]	= Bio::$options['robocassa_password_2'];
		return $matrix;
	}
	static function bio_change_gq_options ($argsInput)
	{
		if ( isset($argsInput["default_course"]))
		{
			Bio::$options["default_course"] = $argsInput["default_course"];
		}
		if ( isset($argsInput["robocassa_key"]))
		{			
			Bio::$options["robocassa_key"] = $argsInput["robocassa_key"];
		}
		if ( isset($argsInput["robocassa_password_1"]))
		{
			Bio::$options["robocassa_password_1"] = $argsInput["robocassa_password_1"];
		}
		if ( isset($argsInput["robocassa_password_2"]))
		{
			Bio::$options["robocassa_password_2"] = $argsInput["robocassa_password_2"];
		}
		update_option(BIO, Bio::$options);
	}
	
	static function add_ctg( $term, $tax_name )
	{
		$price = get_term_meta($term->term_id, "price", true);
		$raiting= get_term_meta($term->term_id, "raiting", true);
		?>	

		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="order">
					<?php echo __("price", BIO);  ?>
				</label> 
			</th>
			<td>
				<input type="number" name="price" value="<?php echo $price; ?>" />
			</td>
		</tr>

		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="raiting">
					<?php echo __("raiting", BIO);  ?>
				</label> 
			</th>
			<td>
				<input type="number" name="raiting" value="<?php echo $raiting; ?>" />
			</td>
		</tr>
		<?php
	}
	static function save_ctg( $term_id ) 
	{
		update_term_meta($term_id, "price", 	$_POST['price']);
		update_term_meta($term_id, "raiting", 	$_POST['raiting']);
		
	}
	static function ctg_columns($theme_columns) 
	{
		$theme_columns['price'] = __("Price", BIO);		
		// $theme_columns['raiting'] = __("Raiting", BIO);		
		return $theme_columns;
	}
	static function manage_ctg_columns($out, $column_name, $term_id) 
	{
		
		switch ($column_name) 
		{
			case 'price': 
				$price = get_term_meta( $term_id, 'price', true ); 
				$out 		.= $price;
				break;	
			case 'raiting': 
				$raiting = get_term_meta( $term_id, 'raiting', true ); 
				$out 		.= $raiting;
				break;
		}
		
		return $out;    
	}
	static function shm_ajax_submit( $returnObj, $params )
	{
		switch($params[0])
		{
			case "reset_current_course":
				$returnObj['do'] =
				[
					"do" => 1
				];
				break;
		}
		return $returnObj;
	}
}