<?php 

class Bio_Quaere extends SMC_Post
{
	static function get_type()
	{
		return BIO_QUAERE_TYPE;
	}
	static function init()
	{
		add_action('init', 						[ __CLASS__, 'register_all' ], 98.5);	
		parent::init();
	}
	static function register_all()
	{
		$labels = array(
			'name'               => __("Quaere", BIO), // Основное название типа записи
			'singular_name'      => __("Quaere", BIO), // отдельное название записи типа Book
			'add_new'            => __("add Quaere", BIO), 
			'all_items' 		 => __('Quaeres', BIO),
			'add_new_item'       => __("add Quaere", BIO), 
			'edit_item'          => __("edit Quaere", BIO), 
			'new_item'           => __("add Quaere", BIO), 
			'view_item'          => __("see Quaere", BIO), 
			'search_items'       => __("search Quaere", BIO), 
			'not_found'          => __("no Quaeres", BIO), 
			'not_found_in_trash' => __("no Quaeres in trash", BIO), 
			'parent_item_colon'  => '',
			'menu_name'          => __("Quaeres", BIO), 
		);
		register_post_type(
			static::get_type(), 
			[
				'labels'             => $labels,
				'taxonomies'		 => [ BIO_FACULTET_TYPE ],
				'public'             => true,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'show_in_menu'       => 'kpdbio_page',
				'query_var'          => true,
				'capability_type'    => 'post',
				'has_archive'        => true,
				'hierarchical'       => false,
				'menu_position'      => 92.5,
				"menu_icon"			 => "dashicons-megaphone",
				'supports'           => array( 'title','editor','author','thumbnail','excerpt','comments' ),
				"rewrite"			 => ["slug" => ""]
			]
		);
	}
}