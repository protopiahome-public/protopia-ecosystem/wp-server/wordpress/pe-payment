<?php

	require_once(__DIR__ . "/../../pe-graphql/vendor/autoload.php");

	use GraphQL\Error\ClientAware;
	use GraphQL\Utils\BuildSchema;
	use GraphQL\Utils\SchemaPrinter;
	use GraphQL\Type\Definition\ObjectType;
	use GraphQL\Type\Definition\Type;

	class PELanding
	{
		static function init()
		{
			add_action( "pe_graphql_make_schema",	[ __CLASS__, "pe_graphql_make_schema"], 20 );
			add_filter(  "smc_add_post_types",	 	[ __CLASS__, "init_obj"], 25 );
			
		}
		
		static function init_obj($init_object)
		{
			$p					= [ ];
			$p['t']				= [ 'type' => 'post' ];	
			$p['class']			= [ 'type' => 'PE_ContactFormMessage' ];
			$p['fields']		= [
				'type' 		=> "cf_message_field", 
				"name" 		=> __( "Fields", BIO )
			];	
			$p['matrix']		= [
				'type' 		=> "cf_message_matrix", 
				"name" 		=> __( "Matrix", BIO ),
				"hidden"	=> true
			];	
			$init_object[ PE_CONTACT_FORM_MESSAGE ]	= $p;
			
			return $init_object;
		}
		
		static function pe_graphql_make_schema()
		{			
			// get landing object by source page id)
			PEGraphql::add_query( 
			'getPE_LandingID', 
			[
				'description' 	=> __( 'Get id of landing object by current source page', BIO ),
				'type' 			=> Type::string(),
				"args"			=> [
					"id"		=> Type::string()
				],
				'resolve' 		=> function( $root, $args, $context, $info )
				{
					$json = get_option( "pe_landings" );
					return $json ? $json[ $args["id"] ] : $args["id"] ;
				}
			]);
			
			
			PEGraphql::add_object_type([
				'name' => 'PE_Landing',
				'description' => __( "landing page generator", BIO ),
				'fields' => apply_filters(
					"pe_landing_fields", 
					[
						"id"	=> Type::string(),
						"json" 	=> Type::string()
					],
					false
				),
				
			]);
			
			PEGraphql::add_input_type( [
				'name' => 'PE_LandingInput',
				'description' 	=> __( "landing page generator", BIO ),
				'fields' => apply_filters(
					"pe_landing_input_fields", 
					[
						"id"	=> Type::string(),
						"json" 	=> Type::string()
					],
					false
				),
			]);
			
			PEGraphql::add_input_type( [
				'name' => 'PE_LandingContactFormMessageInput',
				'description' 	=> __( "Contact For Message", BIO ),
				'fields' => [
						'message_json' 	=> Type::string(),
						"matrix_json"	=> Type::string()
					],
			]);
			
			PEGraphql::add_query( 
			'getPE_Landing', 
			[
				'description' 	=> __( 'Get landing page', BIO ),
				'type' 			=> PEGraphql::object_type( "PE_Landing" ),
				"args"			=> [
					"id"		=> Type::string()
				],
				'resolve' 		=> function( $root, $args, $context, $info )
				{
					$json = get_option( "pe_landing" . $args["id"] );
					return [ "json" => $json ? $json : "" ];
				}
			]);
			
			PEGraphql::add_mutation( 
			'chagePE_Landing', 
			[
				'description' 	=> __( "Change landing page", BIO ),
				'type' 			=>  Type::string(),
				'args'         	=> [
					"id"	=> Type::string(),
					"input"	=> [
						'type' => Type::string()
					]
				],
				'resolve' => function( $root, $args, $context, $info )
				{
					update_option( "pe_landing" . $args["id"] , $args[ 'input' ] );
					return get_option("pe_landing" . $args["id"] );
				}
			]);
			
			/**/
			PEGraphql::add_mutation( 
			'sendPELandingContactFormMessage', 
			[
				'description' 	=> __( "send PELanding Contact form message", BIO ),
				'type' 			=> Type::string(),
				'args'         	=> [
					'input' => [
						'type' => PEGraphql::input_type("PE_LandingContactFormMessageInput"),
					] 
				],
				'resolve' => function( $root, $args, $context, $info )
				{
					$meta = preg_replace( "/'/i", '"',  $args[ 'input' ]['message_json'] );
					$fields = json_decode( $meta );
					for ( $i = 0; $i < count($fields); $i++ )
					{
						if($i == 0)
							$title = $fields[$i]->value;
						switch($fields[$i]->type)
						{
							case "file_loader":
								$content .= "<p><img style='width:100%; max-width:300px;' src='" . $fields[$i]->value . "'></p>";
								break;
							case "string":
							case "time":
							case "phone":
							default:
								$content .= "<p>" . $fields[$i]->label . ": <b>" . $fields[$i]->value . "</b></p>";
						}
					}						
					$da = PE_ContactFormMessage::insert([
						"post_type" 	=> PE_CONTACT_FORM_MESSAGE,
						'post_title' 	=> $title,
						'post_content' 	=> $content,
						"fields" 		=> $args[ 'input' ]['message_json'],
						"matrix"  		=> $args[ 'input' ]['matrix_json']
					]);
					return is_string($da) ? $da : __("Successful", BIO) ;
				}
			]);
			
		}
		
	}