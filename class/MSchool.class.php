<?php
require_once(__DIR__ . "/../../pe-graphql/vendor/autoload.php");

use GraphQL\Error\ClientAware;
use GraphQL\Utils\BuildSchema;
use GraphQL\Utils\SchemaPrinter;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class MSchool
{
	static $options;	
	static $instance;
	static function activate()
	{
		
	}
	static function deactivate()
	{
		
	}
	
	static function get_instance()
	{
		if(!static::$instance)
			static::$instance = new static;
		return static::$instance;
	}
	
	function __construct()
	{	
		static::$options = get_option(PESHOP);
		/*
		add_filter( "bio_gq_options", 					[ __CLASS__, "bio_gq_options"], 51);
		add_filter( "bio_gq_options_input", 			[ __CLASS__, "bio_gq_options_input"], 51);
		add_filter( "bio_get_gq_options", 				[ __CLASS__, "bio_get_gq_options"], 51);
		add_action( "bio_change_gq_options", 			[ __CLASS__, "bio_change_gq_options"], 51);
		*/
		// add Favorites for Bio_Artcles
		add_action( "bio_gq_get_fields", 				[ __CLASS__, "bio_gq_get_fields"], 51, 3 );
		add_action("pe_graphql_make_schema", 			[__CLASS__, "exec_options2"], 10);
		
		add_filter( "smc_add_post_types",	 			[ __CLASS__, "init_obj"], 40);
		add_filter( "bio_get_user",						[ __CLASS__, "bio_get_user"], 20, 2 );
		add_filter( "pe_graphql_user_fields",			[ __CLASS__, "pe_graphql_user_fields"], 20, 2 );
		add_filter( "pe_graphql_get_users",				[ __CLASS__, "pe_graphql_get_users"], 20, 2 );
		add_filter( "pe_graphql_get_user",				[ __CLASS__, "pe_graphql_get_user"], 20);
		add_filter( "bio_facultet_user",				[ __CLASS__, "bio_facultet_user"], 30, 3);
		add_filter( "pe_graphql_change_meta_user",		[ __CLASS__, "pe_graphql_change_meta_user"], 20, 2);
		add_action( "pe_graphql_change_current_user", 	[ __CLASS__, "pe_graphql_change_current_user"]);
		
		add_action( BIO_COURSE_TYPE.'_edit_form_fields',[ __CLASS__, 'add_ctg'], 20, 2 );
		add_action( 'edit_'.BIO_COURSE_TYPE, 			[ __CLASS__, 'save_ctg'], 12);  
		add_action( 'create_'.BIO_COURSE_TYPE, 			[ __CLASS__, 'save_ctg'], 12);
		add_filter( "manage_edit-".BIO_COURSE_TYPE."_columns", 	array( __CLASS__,'ctg_columns')); 
		add_filter( "manage_".BIO_COURSE_TYPE."_custom_column",	array( __CLASS__,'manage_ctg_columns'), 12, 3);	
		
		add_filter( "bio_get_comments_params",			array( __CLASS__, 'bio_get_comments_params'), 12, 2);
		add_action( 'user_register', 					array( __CLASS__, 'user_register') );		
			
	}
	static function user_register( $user_id )
	{
		$avatars	= get_posts([
			"post_type"		=> BIO_AVATAR_TYPE,
			"post_status"	=> "publish",
			"numberposts"	=> 1,
			"orderby"		=> "rand",
			"fields"		=> "ids"
		]);
		if($avatars && count($avatars))
		{
			update_user_meta($user_id, BIO_AVATAR_TYPE, get_the_post_thumbnail_url( $avatars[0], "full"));
		}
	}
	static function bio_get_comments_params( $params)
	{
		if(isset($params['post_id']))
		{
			$facultets = Bio_Facultet::get_article_facultet_ids($params['post_id']);
			$owners = [ ];
			if(is_user_logged_in())
			{
				foreach($facultets as $facultet)
				{				
					$owners[] = (int)get_term_meta($facultet, "post_author", true);
				}
			}
			$owners = array_unique($owners);
			if(!in_array(get_current_user_id(), $owners))
			{
				$owners[] =  get_current_user_id();
				$params['author__in'] = $owners;
				
			}
		}
		return $params;
	}
	static function exec_options2()
	{
		//add Article to Favorite
		PEGraphql::add_mutation( 
			'toggleArticleFavorite', 
			[
				'description' 	=> __( "toggle Article to Favorite", BIO ),
				'type' 			=> Type::boolean(),
				'args'         	=> [
					'id' => [
						'type' 			=> Type::string(), 	
						'description' 	=> __( 'Uniq identificator', BIO ), 'name' => 'id' 
					]
				],
				'resolve' => function( $root, $args, $context, $info ) use ($class_name, $post_type, $val)
				{	
					$user_id 	= get_current_user_id();
					$article	= Bio_Article::get_instance( $args["id"] );
					$is			= $article->is_favorite();
					$article->add_to_favorite( !$is );
					return !$is;
				}
			] 
		);
		
		PEGraphql::add_query( 
			'getMyFavorites', 
			[
				'description' => __( 'Get portal settings', BIO ),
				'type' 		=> Type::listOf( PEGraphql::object_type( "Bio_Article" ) ),
				'args'     	=> [  ],
				'resolve' 	=> function( $root, $args, $context, $info ) use($class_name, $post_type, $val)
				{		
					$favs = Bio_User::get_favorites( get_current_user_id());
					$favorites = [];
					//wp_die($favs);
					if(count( $favs ))
						foreach($favs as $fav)
						{
							if($fav->post_id && $fav->post_id > 0 )
							{
								$article = Bio_Article::get_instance( $fav->post_id );
								if($article->is_enabled())
								{
									$art = Bio_Article::get_single_matrix( $fav->post_id );
									$favorites[] = $art;
								}
							}
						}
					return $favorites;
				}
			] 
		);
		
		
		PEGraphql::add_object_type([
			'name' => 'Bio_Landing',
			'description' => __( "first page", BIO ),
			'fields' => apply_filters(
				"pe_graphql_landing_fields", 
				[
					'background_video' 			=> Type::string(),
					'info_video' 				=> Type::string(),
					'thumbnail' 				=> Type::string(),
					'main_title' 				=> Type::string(),
					'main_description' 			=> Type::string(),
					'info_1_title' 				=> Type::string(),
					'info_1_description' 		=> Type::string(),
					'info_2_title' 				=> Type::string(),
					'info_2_description' 		=> Type::string(),
					'advantages_title' 			=> Type::string(),
					'advantages_description'	=> Type::string(),
					"advantages"				=> Type::listOf( PEGraphql::object_type( "Bio_Advantage" ) ),
					'facultets_title' 			=> Type::string(),
					'facultets_description'		=> Type::string(),
					"facultets"					=> Type::listOf( PEGraphql::object_type( "Bio_Facultet" ) ),
				],
				false
			),
			
		]);
		PEGraphql::add_input_type( 
			[
				"name"		=> 'Bio_LandingInput',
				'description' => __( "first page", BIO ),
				'fields' 		=> apply_filters(
					"pe_graphql_landing_input_fields", 
					[
						'info_video' 				=> Type::string(),
						'background_video' 			=> Type::string(),
						'thumbnail' 				=> Type::string(),
						'main_title' 				=> Type::string(),
						'main_description' 			=> Type::string(),
						'info_1_title' 				=> Type::string(),
						'info_1_description' 		=> Type::string(),
						'info_2_title' 				=> Type::string(),
						'info_2_description' 		=> Type::string(),
						'advantages_title' 			=> Type::string(),
						'advantages_description'	=> Type::string(),
						"advantages"				=> Type::listOf( Type::int() ),
						'facultets_title' 			=> Type::string(),
						'facultets_description'		=> Type::string(),
						"facultets"					=> Type::listOf( Type::int() ),
					],
					false
				),
			]
		);
		PEGraphql::add_mutation( 
			'chageBio_Landing', 
			[
				'description' 	=> __( "first page", BIO ),
				'type' 			=>  Type::boolean(),
				'args'         	=> [
					"input"	=> [
						'type' => PEGraphql::input_type( "Bio_LandingInput" ),
						'description' => __( 'first page data', BIO ),
					]
				],
				'resolve' => function( $root, $args, $context, $info ) use ($class_name, $post_type, $val)
				{
					
					$argsInput = $args['input'];
					if(isset($argsInput['thumbnail'] ))
					{
						$media = Bio_Assistants::insert_media(
							[
								"data" => $argsInput['thumbnail'],
								"media_name"=> 'main_thumbnail.jpg'
							], 
							$term_id
						);
						Bio::$options["thumbnail"] = $argsInput["thumbnail"];
					}
					if ( isset($argsInput["info_video"]))
					{
						Bio::$options["info_video"] = $argsInput["info_video"];
					}
					if ( isset($argsInput["background_video"]))
					{
						Bio::$options["background_video"] = $argsInput["background_video"];
					}
					if ( isset($argsInput["info_video"]))
					{
						Bio::$options["info_video"] = $argsInput["info_video"];
					}
					if ( isset($argsInput["main_title"]))
					{
						Bio::$options["main_title"] = $argsInput["main_title"];
					}
					if ( isset($argsInput["main_description"]))
					{
						Bio::$options["main_description"] = $argsInput["main_description"];
					}
					if ( isset($argsInput["info_1_title"]))
					{
						Bio::$options["info_1_title"] = $argsInput["info_1_title"];
					}
					if ( isset($argsInput["info_1_description"]))
					{
						Bio::$options["info_1_description"] = $argsInput["info_1_description"];
					}
					if ( isset($argsInput["info_2_title"]))
					{
						Bio::$options["info_2_title"] = $argsInput["info_2_title"];
					}
					if ( isset($argsInput["info_2_description"]))
					{
						Bio::$options["info_2_description"] = $argsInput["info_2_description"];
					}
					if ( isset($argsInput["advantages_title"]))
					{
						Bio::$options["advantages_title"] = $argsInput["advantages_title"];
					}
					if ( isset($argsInput["advantages_description"]))
					{
						Bio::$options["advantages_description"] = $argsInput["advantages_description"];
					}
					if ( isset($argsInput["facultets_title"]))
					{
						Bio::$options["facultets_title"] = $argsInput["facultets_title"];
					}
					if ( isset($argsInput["facultets_description"]))
					{
						Bio::$options["facultets_description"] = $argsInput["facultets_description"];
					}
					update_option(BIO, Bio::$options);
					
					return true;
				}
			] 
		);
		
		PEGraphql::add_query( 
			'getBio_Landing', 
			[
				'description' 	=> __( 'Get portal settings', BIO ),
				'type' 			=> PEGraphql::object_type( "Bio_Landing" ),
				"args"			=> [
					"paging" 		=> [ "type" => PEGraphql::input_type("Paging") ]
				],
				'resolve' 		=> function( $root, $args, $context, $info ) use($class_name, $post_type, $val)
				{		
					$obj = [
						"background_video"			=> Bio::$options['background_video'],
						"info_video"				=> Bio::$options['info_video'],
						"thumbnail"					=> wp_get_attachment_url(Bio::$options['thumbnail']),
						"main_title"				=> Bio::$options['main_title'],
						"main_description"			=> Bio::$options['main_description'],
						"info_1_title"				=> Bio::$options['info_1_title'],
						"info_1_description"		=> Bio::$options['info_1_description'],
						"info_2_title"				=> Bio::$options['info_2_title'],
						"info_2_description"		=> Bio::$options['info_2_description'],
						"advantages_title"			=> Bio::$options['advantages_title'],
						"advantages_description"	=> Bio::$options['advantages_description'],
						"facultets_title"			=> Bio::$options['facultets_title'],
						"facultets_description"		=> Bio::$options['facultets_description'],
						"facultets"					=> Bio_Facultet::get_all_matrixes( $args["paging"] ),
						"advantages"				=> Bio_Advantage::get_all_matrixes( $args["paging"] ),
					];
					return $obj;
				}
			] 
		);
	}
	static function bio_gq_get_fields( $fields, $post_class, $post_type)
	{
		if($post_type == BIO_ARTICLE_TYPE )
		{
			$fields['favorite'] = [
				'type' 			=>  Type::boolean(),
				'description' 	=> "is this Article added to User's Favorites",
				"resolve" 		=>  function( $root, $args, $context, $info ) 
				{
					$article 	= Bio_Article::get_instance( $root['id'] );		
					//wp_die($root['id']);					
					return $article->is_favorite();
				}
			];
		}
		if($post_type == BIO_FACULTET_TYPE )
		{
			$fields['price'] = [
				'type' 			=>  Type::int(),
				'description' 	=> "Price of Facultet access.",
				"resolve" 		=>  function( $root, $args, $context, $info ) 
				{
					$article 	= Bio_Facultet::get_instance( $root['id'] );		
					//wp_die($root['id']);					
					return $article->get_meta("price");
				}
			];
		}
		return $fields;
	}
	static function bio_gq_options( $matrix )
	{		
		$matrix['background_video']		= [
			'type' => Type::string(), 	
			'description' 	=> __( 'background_video', BIO ) 
		];
		$matrix['thumbnail']		= [
			'type' => Type::string(), 	
			'description' 	=> __( 'thumbnail', BIO ) 
		];
		$matrix['info_video']			= [
			'type' => Type::string(), 	
			'description' 	=> __( 'info_video', BIO ) 
		];
		$matrix['main_title']			= [
			'type' => Type::string(), 	
			'description' 	=> __( 'main_title', BIO ) 
		];
		$matrix['main_description']			= [
			'type' => Type::string(), 	
			'description' 	=> __( 'main_description', BIO ) 
		];
		$matrix['info_1_title']			= [
			'type' => Type::string(), 	
			'description' 	=> __( 'info_1_title', BIO ) 
		];
		$matrix['info_1_description']			= [
			'type' => Type::string(), 	
			'description' 	=> __( 'info_1_description', BIO ) 
		];
		$matrix['info_2_title']			= [
			'type' => Type::string(), 	
			'description' 	=> __( 'info_2_title', BIO ) 
		];
		$matrix['info_2_description']			= [
			'type' => Type::string(), 	
			'description' 	=> __( 'info_2_description', BIO ) 
		];
		$matrix['advantages_title']			= [
			'type' => Type::string(), 	
			'description' 	=> __( 'advantages_title', BIO ) 
		];
		$matrix['advantages_description']	= [
			'type' => Type::string(), 	
			'description' 	=> __( 'advantages_description', BIO ) 
		];
		$matrix['facultets_title']	= [
			'type' => Type::string(), 	
			'description' 	=> __( 'facultets_title', BIO ) 
		];
		$matrix['facultets_description']	= [
			'type' => Type::string(), 	
			'description' 	=> __( 'facultets_description', BIO ) 
		];
		return $matrix;
	}
	static function bio_gq_options_input( $matrix )
	{		
		$matrix['background_video']		= [
			'type' => Type::string(), 	
			'description' 	=> __( 'background_video', BIO ) 
		
		];
		$matrix['thumbnail']		= [
			'type' => Type::string(), 	
			'description' 	=> __( 'thumbnail', BIO ) 
		
		];
		$matrix['info_video']			= [
			'type' => Type::string(), 	
			'description' 	=> __( 'info_video', BIO ) 
		];
		
		$matrix['main_title']			= [
			'type' => Type::string(), 	
			'description' 	=> __( 'main_title', BIO ) 
		];
		$matrix['main_description']			= [
			'type' => Type::string(), 	
			'description' 	=> __( 'main_description', BIO ) 
		];
		$matrix['info_1_title']			= [
			'type' => Type::string(), 	
			'description' 	=> __( 'info_1_title', BIO ) 
		];
		$matrix['info_1_description']			= [
			'type' => Type::string(), 	
			'description' 	=> __( 'info_1_description', BIO ) 
		];
		$matrix['info_2_title']			= [
			'type' => Type::string(), 	
			'description' 	=> __( 'info_2_title', BIO ) 
		];
		$matrix['info_2_description']			= [
			'type' => Type::string(), 	
			'description' 	=> __( 'info_2_description', BIO ) 
		];
		$matrix['advantages_title']			= [
			'type' => Type::string(), 	
			'description' 	=> __( 'advantages_title', BIO ) 
		];
		$matrix['advantages_description']	= [
			'type' => Type::string(), 	
			'description' 	=> __( 'advantages_description', BIO ) 
		];
		$matrix['facultets_title']	= [
			'type' => Type::string(), 	
			'description' 	=> __( 'facultets_title', BIO ) 
		];
		$matrix['facultets_description']	= [
			'type' => Type::string(), 	
			'description' 	=> __( 'facultets_description', BIO ) 
		];
		return $matrix;
	}
	static function bio_get_gq_options( $matrix )
	{
		$matrix['thumbnail']			= wp_get_attachment_url( Bio::$options['thumbnail'] );
		$matrix['background_video']		= Bio::$options['background_video'];
		$matrix['info_video']			= Bio::$options['info_video'];
		$matrix['main_title']			= Bio::$options['main_title'];
		$matrix['main_description']		= Bio::$options['main_description'];
		$matrix['info_1_title']			= Bio::$options['info_1_title'];
		$matrix['info_1_description']	= Bio::$options['info_1_description'];
		$matrix['info_2_title']			= Bio::$options['info_2_title'];
		$matrix['info_2_description']	= Bio::$options['info_2_description'];
		$matrix['advantages_title']		= Bio::$options['advantages_title'];
		$matrix['advantages_description']= Bio::$options['advantages_description'];
		$matrix['facultets_title']		= Bio::$options['facultets_title'];
		$matrix['facultets_description']= Bio::$options['facultets_description'];
		
		return $matrix;
	}
	static function bio_change_gq_options ($argsInput)
	{
		if(isset($argsInput['thumbnail'] ))
		{
			$media = Bio_Assistants::insert_media(
				[
					"data" => $argsInput['thumbnail'],
					"media_name"=> 'main_thumbnail.jpg'
				], 
				$term_id
			);
			Bio::$options["thumbnail"] = $argsInput["thumbnail"];
		}
		if ( isset($argsInput["background_video"]))
		{
			Bio::$options["background_video"] = $argsInput["background_video"];
		}
		if ( isset($argsInput["info_video"]))
		{
			Bio::$options["info_video"] = $argsInput["info_video"];
		}
		if ( isset($argsInput["main_title"]))
		{
			Bio::$options["main_title"] = $argsInput["main_title"];
		}
		if ( isset($argsInput["main_description"]))
		{
			Bio::$options["main_description"] = $argsInput["main_description"];
		}
		if ( isset($argsInput["info_1_title"]))
		{
			Bio::$options["info_1_title"] = $argsInput["info_1_title"];
		}
		if ( isset($argsInput["info_1_description"]))
		{
			Bio::$options["info_1_description"] = $argsInput["info_1_description"];
		}
		if ( isset($argsInput["info_2_title"]))
		{
			Bio::$options["info_2_title"] = $argsInput["info_2_title"];
		}
		if ( isset($argsInput["info_2_description"]))
		{
			Bio::$options["info_2_description"] = $argsInput["info_2_description"];
		}
		if ( isset($argsInput["advantages_title"]))
		{
			Bio::$options["advantages_title"] = $argsInput["advantages_title"];
		}
		if ( isset($argsInput["advantages_description"]))
		{
			Bio::$options["advantages_description"] = $argsInput["advantages_description"];
		}
		if ( isset($argsInput["facultets_title"]))
		{
			Bio::$options["facultets_title"] = $argsInput["facultets_title"];
		}
		if ( isset($argsInput["facultets_description"]))
		{
			Bio::$options["facultets_description"] = $argsInput["facultets_description"];
		}
		update_option(BIO, Bio::$options);
	}
	
	
	static function init_obj($init_object)
	{
		$point					= [];
		$point['t']				= ['type' => 'post'];	
		$point['class']			= ['type' => 'Bio_Advantage'];
		$point['order']			= ['type' => "number",  "name" => __("Order", BIO)];
		$point['is_main_page']	= ['type' => "boolean", "name" => __("To main page", BIO)];
		$point[BIO_FACULTET_TYPE] = [
			'type' => "taxonomy", 
			'smc_post'=> "Bio_Facultet", 
			"object" => BIO_FACULTET_TYPE, 
			"name" => __("Facultet", BIO)
		];
		$init_object[BIO_ADVANTAGE_TYPE]	= $point;
		
		//
		$init_object[BIO_COURSE_TYPE]['logotype'] = [
			'type'		=> "picto",
			"object" 	=> "media",  
			"name"		=> __("Logotype", BIO),
		];
		$init_object[BIO_COURSE_TYPE]['logotype_name']	= ['type' => "string", "name" => __("Logotype's name", BIO)];
		$init_object[BIO_COURSE_TYPE]['includes']		= ['type' => "string", "name" => __("includes url", BIO)];
		$init_object[BIO_COURSE_TYPE]['quote']			= ['type' => "string", "name" => __("Short Quote", BIO)];
		$init_object[BIO_COURSE_TYPE]['jitsi']			= ['type' => "string", "name" => __("Talk room ID", BIO)];
		$init_object[BIO_COURSE_TYPE]['jitsi_password']	= ['type' => "string", "name" => __("Talk room password", BIO)];
		
		
		$init_object[BIO_ARTICLE_TYPE]['quote']			= ['type' => "text", "name" => __("Short Quote", BIO)];
		$init_object[BIO_ARTICLE_TYPE]['video']			= ['type' => "string", "name" => __("video url", BIO)];
		
		
		$point					= [];
		$point['t']				= ['type' => 'post'];	
		$point['class']			= ['type' => 'Bio_Tarif'];
		$point['payment']		= ['type' => "number", "name" => __("payment", BIO)];
		$init_object[BIO_TARIF_TYPE]	= $point;
		
		$point					= [];
		$point['t']				= ['type' => 'post'];	
		$point['class']			= ['type' => 'Bio_Notification'];
		$init_object[BIO_NOTIFICATION_TYPE]	= $point;

		$point					= [];
		$point['t']				= ['type' => 'post'];	
		$point['class']			= ['type' => 'Bio_Avatar'];
		$init_object[BIO_AVATAR_TYPE]	= $point;

		$point					= [];
		$point['t']				= ['type' => 'post'];	
		$point['class']			= ['type' => 'Bio_ImageResult'];
		$init_object[BIO_IMAGE_RESULT_TYPE]	= $point;

		$point					= [];
		$point['t']				= ['type' => 'post'];	
		$point['class']			= ['type' => 'Bio_Quaere'];
		$point['adressee']		= ['type' => "user",  "name" => __("Adressee", BIO)];
		$init_object[BIO_QUAERE_TYPE]	= $point;

		$point					= [];
		$point['t']				= ['type' => 'post'];	
		$point['class']			= ['type' => 'Bio_Comment'];
		$init_object[BIO_COMMENT_TYPE]	= $point;
		
		
		$init_object[BIO_FACULTET_TYPE][BIO_ADVANTAGE_TYPE]	= [
			'type' 		=> "array", 
			'class'		=> "Bio_Advantage", 
			"object" 	=> BIO_ADVANTAGE_TYPE, 
			"order"		=> "ASC",
			"orderby"	=> "order",
			"name" 		=> __("Advantages", BIO)
		];
		$init_object[BIO_FACULTET_TYPE][BIO_IMAGE_RESULT_TYPE]	= [
			'type' 		=> "array", 
			'class'		=> "Bio_ImageResult", 
			"object" 	=> BIO_IMAGE_RESULT_TYPE, 
			"name" 		=> __("Image Results", BIO)
		];		
		$init_object[BIO_FACULTET_TYPE]['price'] = ['type' => "number", "name" => __("Price", PESHOP)];
		
		
		return $init_object;
	}
	
	
	static function bio_get_user($args, $user)
	{
		$avatar						= get_user_meta($user->ID, "avatar", true);
		$args['avatar']				= $avatar ? get_bloginfo("url").$avatar : "";
		$avatar_id					= get_user_meta($user->ID, BIO_AVATAR_TYPE, true);
		$args['bio_avatar']			= Bio_Avatar::get_single_matrix( $avatar_id );
		$tarif_id					= get_user_meta($user->ID, "bio_tarif", true);
		$args['bio_tarif']			= Bio_Tarif::get_single_matrix($tarif_id);
		$args['finish_tarif_date']		= (int)get_user_meta($user->ID, "finish_tarif_date", true);
		return $args;
	}
	static function pe_graphql_user_fields( $fields, $isForInput )
	{
		$fields['bio_avatar'] = $isForInput ? Type::int() : PEGraphql::object_type( "Bio_Avatar" );		
		$fields['bio_tarif'] 	= $isForInput ? Type::int() : PEGraphql::object_type( "Bio_Tarif" );
		$fields['finish_tarif_date'] = Type::int() ;
		//
		return $fields;
	}
	static function bio_facultet_user($matrix, $facultet_data, $user)
	{
		if( time() - strtotime( $facultet_data->date ) > MONTH_IN_SECONDS )
		{
			Bio_Facultet::remove_user_facultet( $matrix['id'], $user->ID );
			return false;
		}
		return $matrix;
	}
	static function pe_graphql_get_user($user)
	{
		$avatar						= get_user_meta($user->ID, "avatar", true);
		$user->avatar				= $avatar ? get_bloginfo("url").$avatar : "";
		$avatar_id					= get_user_meta($user->ID, "bio_avatar", true);
		$user->bio_avatar			= Bio_Avatar::get_single_matrix( $avatar_id );
		$tarif_id					= get_user_meta($user->ID, "bio_tarif", true);
		$user->bio_tarif			= Bio_Tarif::get_single_matrix($tarif_id);
		$user->finish_tarif_date	= (int)get_user_meta($user->ID, "finish_tarif_date", true);
		return $user;
	}
	static function pe_graphql_get_users($users)
	{
		foreach($users as $user)
		{
			$user = static::pe_graphql_get_user($user);
		}
		return $users;
	}
	static function pe_graphql_change_meta_user($user_id, $fields)
	{
		if( isset($fields['input']['bio_avatar']) )
		{
			update_user_meta($user_id, "bio_avatar", $fields['input']['bio_avatar'] );
		}
		if( isset($fields['input']['bio_tarif']) )
		{
			update_user_meta($user_id, "bio_tarif", $fields['input']['bio_tarif'] );
		}
		if( isset($fields['input']['finish_tarif_date']) )
		{
			//wp_die([ $user_id, $fields['input']['finish_tarif_date'] ]);
			update_user_meta($user_id, "finish_tarif_date", $fields['input']['finish_tarif_date'] );
		}
		return $user_id;
	}
	static function pe_graphql_change_current_user($fields)
	{
		if(isset($fields['input']['bio_avatar']))
		{
			update_user_meta(get_current_user_id(), "bio_avatar", $fields['input']['bio_avatar'] );
		}
		if(isset($fields['input']['bio_tarif']))
		{
			update_user_meta(get_current_user_id(), "bio_tarif", $fields['input']['bio_tarif'] );
		}
		if(isset($fields['input']['finish_tarif_date']))
		{
			update_user_meta(get_current_user_id(), "finish_tarif_date", $fields['input']['finish_tarif_date'] );
		}
	}
	
	
	
	
	
	
	static function add_ctg( $term, $tax_name )
	{
		$logotype 			= get_term_meta($term->term_id, "logotype", true);
		$quote				= get_term_meta($term->term_id, "quote", true);
		$jitsi				= get_term_meta($term->term_id, "jitsi", true);
		$jitsi_password		= get_term_meta($term->term_id, "jitsi_password", true);
		?>	

		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="logotype">
					<?php echo __("Logotype", BIO);  ?>
				</label> 
			</th>
			<td>
				<?php
					echo get_input_file_form2( "group_icon", $logotype, "group_icon", "logotype" );
				?>
			</td>
		</tr>
		
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="order">
					<?php echo __("quote", BIO);  ?>
				</label> 
			</th>
			<td>
				<textarea rows="6" name="quote" class="form-control"><?php echo $quote; ?></textarea>
			</td>
		</tr>
		
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="jitsi">
					<?php echo __("Talk Room ID", BIO);  ?>
				</label> 
			</th>
			<td>
				<input type="text" rows="6" name="jitsi" class="form-control" value='<?php echo $jitsi; ?>' />
			</td>
		</tr>
		
		<tr class="form-field">
			<th scope="row" valign="top">
				<label for="jitsi_password">
					<?php echo __("Talk Room Password", BIO);  ?>
				</label> 
			</th>
			<td>
				<input type="text" rows="6" name="jitsi_password" class="form-control" value='<?php echo $jitsi_password; ?>' />
			</td>
		</tr>
		<?php
	}
	static function save_ctg( $term_id ) 
	{
		update_term_meta($term_id, "quote", 		$_POST['quote']);
		update_term_meta($term_id, "jitsi", 		$_POST['jitsi']);
		update_term_meta($term_id, "jitsi_password",$_POST['jitsi_password']);
		if(!$_POST['group_icon0']) return;
			update_term_meta($term_id, "logotype",  $_POST['group_iconlogotype']);
		
	}
	static function ctg_columns($theme_columns) 
	{
		$theme_columns['logotype'] = __("logotype", BIO);		
		return $theme_columns;
	}
	static function manage_ctg_columns($out, $column_name, $term_id) 
	{
		
		switch ($column_name) 
		{
			case 'logotype': 
				$logotype = get_term_meta( $term_id, 'logotype', true ); 
				$out 		.= "<img src='" . wp_get_attachment_url($logotype) . "' style='width:auto; height:60px;' />";
				break;
		}
		
		return $out;    
	}
	static function set_user_facultet( $user_id, $term_id )
	{
		Bio_Facultet::add_user_facultet($term_id, $user_id);
	}
}