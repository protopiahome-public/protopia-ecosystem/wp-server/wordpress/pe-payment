<?php
error_reporting(E_ALL);
/*
Plugin Name: PE payment
Plugin URI: 
Description: PE payment
Version: 0.0.121
Author: Genagl
Author URI: http://genagl.ru/author
Contributors: genag1@list
Text Domain:   mcourses
Domain Path:   /lang/
*/

//библиотека переводов
function init_textdomain_mcourses() 
{ 
	if (function_exists('load_plugin_textdomain')) 
	{
		load_plugin_textdomain("mcourses", false , dirname( plugin_basename( __FILE__ ) ) .'/lang/');     
	} 
}
add_action('plugins_loaded', 'init_textdomain_mcourses');


require_once ABSPATH . 'wp-admin/includes/plugin.php';
if( !is_plugin_active( "pe-edu/bo.plugin.php" ) )
{
	return;
}

define('MCOURSES_URLPATH', WP_PLUGIN_URL.'/pe_payment/');
define('MCOURSES_REAL_PATH', WP_PLUGIN_DIR.'/'.plugin_basename(dirname(__FILE__)).'/');
define('MCOURSES', 'mcourses');
define('M_PAYMENT_TYPE', 'm_payment');
define('BIO_TESTINOMIAL_TYPE', 'bio_testinomial');
define('BIO_REVIEW_TYPE', 'bio_review');
define('BIO_QUAERE_TYPE', 'bio_quaere');
define('PE_CONTACT_FORM_MESSAGE', 'pe_cf_message');

require_once(MCOURSES_REAL_PATH . 'class/pe_payment.class.php');
require_once(MCOURSES_REAL_PATH . 'class/MSchool.class.php');


if (function_exists('register_activation_hook'))
{
	register_activation_hook( __FILE__, array( "pe_payment", 'activate' ) );
	register_activation_hook( __FILE__, array( "MSchool", 'activate' ) );
	
}
if (function_exists('register_deactivation_hook'))
{
	register_deactivation_hook(__FILE__, array("pe_payment", 'deactivate'));
	register_deactivation_hook(__FILE__, array("MSchool", 'deactivate'));
}
add_action("init", "init_pe_payment", 2);
add_action( 'plugins_loaded', 'pe_payment_action_function_name', 11 );
function init_pe_payment()
{
	pe_payment::get_instance();
	MSchool::get_instance();
	MPayment::init();
	Bio_Testinomial::init();
	Bio_Review::init();
	Bio_Quaere::init();
	PELanding::init();
	PE_ContactFormMessage::init();
}
function pe_payment_action_function_name() 
{
	if(!class_exists("SMC_Post") )
	{		
		require_once(BIO_REAL_PATH.'class/SMC_Post.php');
		require_once(BIO_REAL_PATH.'class/SMC_Taxonomy.class.php');
	}
	require_once(MCOURSES_REAL_PATH.'class/MPayment.class.php');
	require_once(MCOURSES_REAL_PATH.'class/Bio_Testinomial.class.php');
	require_once(MCOURSES_REAL_PATH.'class/Bio_Review.class.php');
	require_once(MCOURSES_REAL_PATH.'class/Bio_Quaere.class.php');
	require_once(MCOURSES_REAL_PATH.'class/Bio_Quaere.class.php');
	require_once(MCOURSES_REAL_PATH.'class/PELanding.gq.php');
	require_once(MCOURSES_REAL_PATH.'class/PE_ContactFormMessage.class.php');
	
}